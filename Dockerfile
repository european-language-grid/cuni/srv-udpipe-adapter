# Use an official Python 3.7 runtime with slim Stretch Debian
FROM python:3.7-slim-stretch

# Install required packages
RUN pip3 install aiohttp

# Set the working directory to /udpipe
WORKDIR /udpipe

# Copy the required files to /udpipe
COPY LICENSE README.md udpipe_elg_rest_server.py /udpipe/

# Expose the REST server port
EXPOSE 8080

# Run the server when the container launches
CMD ["/usr/bin/env", "python3", "/udpipe/udpipe_elg_rest_server.py", "--port=8080", "--max_request_size=1024000", "--udpipe_host=localhost", "--udpipe_port=8001"]
