#!/usr/bin/env python3
import asyncio
import json
import uuid

import aiormq
import yaml
import yarl

futures = {}

async def on_response(message):
    future = futures.pop(message.header.properties.correlation_id)
    future.set_result(message.body)

async def main():
    # Load ELG credentials
    with open("mq.yaml", "r") as mq_yaml:
        credentials = yaml.safe_load(mq_yaml.read())

    # Open connection to RabbitMQ
    connection = await aiormq.connect(yarl.URL.build(
        scheme="amqp", host=credentials["host"], port=credentials["port"],
        user=credentials["username"], password=credentials["password"]))
    channel = await connection.channel()

    # Create listening queue
    declare_ok = await channel.queue_declare(exclusive=True, auto_delete=True)

    # Register the listening part
    await channel.basic_consume(declare_ok.queue, on_response)

    # Perform the request
    correlation_id = str(uuid.uuid4())
    future = loop.create_future()
    futures[correlation_id] = future

    await channel.basic_publish(
        json.dumps({"metadata": [5,6,{"5":"7"}], "request":{"type":"text", "content": "Hello. How are you?"}}).encode(),
        routing_key=credentials["topic_in"],
        properties=aiormq.spec.Basic.Properties(content_type="application/json", correlation_id=correlation_id, reply_to=declare_ok.queue),
    )

    result = await future
    print(result.decode())

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
