#!/usr/bin/env python3
import argparse
import asyncio
import http.server
import json
import udpipe_elg_adapter

parser = argparse.ArgumentParser()
parser.add_argument("--host", default="localhost", type=str, help="Host of running UDPipe server")
parser.add_argument("--model", default="en", type=str, help="UDPipe model to use")
parser.add_argument("--port", default=8001, type=int, help="Port of running UDPipe server")
args = parser.parse_args()

adapter = udpipe_elg_adapter.UDPipeElgAdapter(args.host, args.port, args.model)

class Handler(http.server.BaseHTTPRequestHandler):
    def do_POST(self):
        self.send_response(200)
        self.send_header("Content-Type", "application/json")
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()

        request = self.rfile.read(int(self.headers.get("content-length", "-1"))).decode()
        content_type = self.headers.get("content-type", "application/json")
        if "text/plain" in content_type:
            request = json.dumps({"request": {"type": "text", "content": request}})
        self.wfile.write(asyncio.get_event_loop().run_until_complete(adapter.process_elg_request(request)).encode())

server = http.server.HTTPServer(("localhost", 8002), Handler)
server.serve_forever()
