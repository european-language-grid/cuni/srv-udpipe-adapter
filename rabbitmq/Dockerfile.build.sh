#!/bin/sh

first_docker_build_args="--pull"

for model in af ar be bg ca cop cs cu da de el en es et eu fa fi fr fro ga gl \
  got grc he hi hr hu hy id it ja ko la lt lv lzh mr mt nb nl nn orv pl pt ro \
  ru se sk sl sr sv ta te tr ug uk ur vi wo zh; do
  dir=srv-udpipe-adapter-$model

  mkdir $dir
  sed "s/UDPIPE_MODEL/$model/g" Dockerfile.template >$dir/Dockerfile
  cp udpipe_elg_adapter.py udpipe_elg_rabbitmq_server.py $dir

  tag="$CI_REGISTRY_IMAGE/$model"

  docker build $first_docker_build_args -t "$tag" $dir
  first_docker_build_args=

  docker push "$tag"

  rm -r $dir
done
